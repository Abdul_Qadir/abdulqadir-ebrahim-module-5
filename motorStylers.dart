import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'usersList.dart';

class AddName extends StatefulWidget {
  const AddName({Key? key}) : super(key: key);

  @override
  State<AddName> createState() => _AddNameState();
}


class _AddNameState extends State<AddName> {
  @override
  Widget build(BuildContext context) {
    TextEditingController firstNameController = TextEditingController();
    TextEditingController lastNameController = TextEditingController();
    TextEditingController emailController = TextEditingController();

    Future _addUser() {
      final firstName = firstNameController.text;
      final lastName = lastNameController.text;
      final email = emailController.text;

      final ref = FirebaseFirestore.instance.collection("Users").doc();

      return ref
          .set({
            "fistName": firstName,
            "lastName": lastName,
            "email": email,
          })
          .then((value) => {
                firstNameController.text = "",
                lastNameController.text = "",
                emailController.text = "",
              })
          .catchError((onError) => print("onError"));
    }

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: firstNameController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'First Name',
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: lastNameController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'Last Name',
              )),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: emailController,
              decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'Email',
              )),
        ),
        ElevatedButton(
            onPressed: () {
              _addUser();
            },
            child: const Text('Submit')),
      ],
    );
  }
}


