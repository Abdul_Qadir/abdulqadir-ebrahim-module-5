import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class UsersList extends StatefulWidget {
  const UsersList({Key? key}) : super(key: key);

  @override
  State<UsersList> createState() => _UsersListState();
}


class _UsersListState extends State<UsersList> {
  final Stream<QuerySnapshot> _myUsers =
      FirebaseFirestore.instance.collection('Users').snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _firstNameFieldCtrlr = TextEditingController();
    TextEditingController _lastNameFieldCtrlr = TextEditingController();
    TextEditingController _emailFieldCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection('Users')
          .doc(docId)
          .delete()
          .then((value) => print('Deleted'));
    }

    void _update(data) {
      var users = FirebaseFirestore.instance.collection('Users');
      //print(data);
      _firstNameFieldCtrlr.text = data["firstName"];
      _lastNameFieldCtrlr.text = data["lastName"];
      _emailFieldCtrlr.text = data["email"];
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text("Update"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _firstNameFieldCtrlr,
                  ),
                  TextField(
                    controller: _lastNameFieldCtrlr,
                  ),
                  TextField(
                    controller: _emailFieldCtrlr,
                  ),
                  TextButton(
                      onPressed: () {
                        users.doc(data["doc_id"]).update({
                          "fistName": _firstNameFieldCtrlr.text,
                          "lastName": _lastNameFieldCtrlr.text,
                          "email": _emailFieldCtrlr.text,
                        });
                        Navigator.pop(context);
                      },
                      child: const Text('Update'))
                ]),
              ));
    }

    return StreamBuilder(
      stream: _myUsers,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Users not available");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;
                          return ListTile(
                            title: Text(data['firstName']),
                            subtitle: Text(data['lastName']),
                          );
                          ButtonTheme(
                              child: ButtonBar(
                            children: [
                              OutlineButton.icon(
                                onPressed: () {
                                  _update(data);
                                },
                                icon: const Icon(Icons.edit),
                                label: const Text('Edit'),
                              ),
                              OutlineButton.icon(
                                onPressed: () {
                                  _delete(data['doc_id']);
                                },
                                icon: const Icon(Icons.remove),
                                label: const Text('Delete'),
                              )
                            ],
                          ));
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (const Text('No Data'));
        }
      },
    );
  }
}
