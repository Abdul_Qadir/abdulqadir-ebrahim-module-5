import 'package:flutter/material.dart';
import 'motorStylers.dart';
import 'package:firebase_core/firebase_core.dart';
import 'usersList.dart';


Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: "AIzaSyBsxy8o3SPTPlh90R55Vx4BD7smaOsvA4w",
        authDomain: "aqe-module-4.firebaseapp.com",
        databaseURL: "https://aqe-module-4-default-rtdb.firebaseio.com",
        projectId: "aqe-module-4",
        storageBucket: "aqe-module-4.appspot.com",
        messagingSenderId: "910828007471",
        appId: "1:910828007471:web:9ea9136638b3fd50ddd6da"),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Motor Stylers',
      theme: ThemeData(
        primarySwatch: Colors.indigo,
      ),
      home: const MyHomePage(title: 'Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: const <Widget>[AddName(),
          UsersList(),
          ],
        ),
      ),
    ));
  }
}
